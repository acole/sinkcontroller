/*
	Sink Controller

	This is a controller to activate a transfer pump motor for a camp
  sink when someone walks up.

	The circuit:
	* Arduino Nano or similiar
	* HC-SR04 Ultrasonic Sensor
  * Single Channel or similiar Relay Module
  * 12V DC Transfer Pump

	Created: 06/13/19
	By: Austin Cole
	Modified: 06/13/19
	By: Austin Cole

	https://gitlab.com/acole/sinkcontroller

*/

#include <Arduino.h>
#include <NewPing.h>

// Relay Settings
int r1 = 2; // Define pin for relay

// Distance Triggers
int r1On = 30; // Set distance to turn motor on in cm

// Sensor Settings
#define TRIGGER_PIN 3   // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN 4      // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE 400 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

void setup()
{
  Serial.begin(115200); // Open serial monitor at 115200 baud to see ping results.
  pinMode(r1, OUTPUT);
}

void loop()
{
  delay(500); // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
  int md = sonar.ping_cm();
  Serial.print("Ping: ");
  Serial.print(md); // Send ping, get distance in cm and print result (0 = outside set distance range)
  Serial.println("cm");

  // Turn relay on/off depending on motion state
  if (md <= r1On && md > 0)
  {
    digitalWrite(r1, LOW);
    // Serial.print ln("Relay On");
  }
  else
  {
    digitalWrite(r1, HIGH);
    // Serial.println("Relay Off");
  }
}